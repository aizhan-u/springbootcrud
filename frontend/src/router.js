import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      name: 'Items',
      path: '/',
      component: () => import('@/components/Index'),
    },
    {
      name: 'Item',
      path: '/item',
      props: route => ({ action: route.query.action, id: route.query.id }),
      component: () => import('@/components/Item')
    }
  ],
})
