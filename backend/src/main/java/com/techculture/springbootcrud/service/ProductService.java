package com.techculture.springbootcrud.service;

import com.techculture.springbootcrud.entity.Product;
import com.techculture.springbootcrud.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductService {
    @Autowired
    private ProductRepository repo;

    public Product saveProduct(Product p) {
        return repo.save(p);
    }

    public List<Product> getProducts() {
        return repo.findAll();
    }

    public Product getProductById(int id) {
        return repo.findById(id).orElse(null);
    }

    public String deleteProduct(int id) {
        repo.deleteById(id);
        return "Success";
    }

    public Product updateProduct(Product p) {
        Product current = repo.findById(p.getId()).orElse(null);
        current.setName(p.getName());
        current.setQuantity(p.getQuantity());
        current.setPrice(p.getPrice());
        return repo.save(current);
    }
}
